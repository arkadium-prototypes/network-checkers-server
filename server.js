var WebSocketServer = require("ws").Server;

var wss = new WebSocketServer({port: process.env.PORT || 9090});

var users = []; 

var failedConnectionList = [];
var connectionAttemptList = [];

var UserPair = function (firstUser, secondUser) {
    this.firstUser = firstUser;
    this.secondUser = secondUser;
};

var User = function (name, connection) {
    this.name = name;
    this.opponentName = null;
    this.connection = connection;
};

var connectionHasFailedPreviously = function (firstUser, secondUser) {
    if (firstUser == secondUser) { return true; } // edge case where the names are the same for some reason
    for (var i = 0; i < failedConnectionList.length; ++i) {
        var userPair = failedConnectionList[i];
        if ((userPair.firstUser == firstUser || userPair.secondUser == firstUser) &&
            (userPair.firstUser == secondUser || userPair.secondUser == secondUser)) {
            return true;
        }
    }
    return false;
};

var userAlreadyAttempingConnection = function (username) {
    for (var i = 0; i < connectionAttemptList.length; ++i) {
        var userPair = connectionAttemptList[i];
        if (userPair.firstUser == username || userPair.secondUser == username) {
            return true;
        }
    }
    return false;
};

var getUserByName = function (name) {
    var result = null;
    for (var i = 0; i < users.length; ++i) {
        var user = users[i];
        if (user.name == name) {
            result = user;
        }
    }
    return result;
}

var getUserConnectionByName = function (name) {
    var user = getUserByName(name);
    if (user) {
        return user.connection;
    }
    else {
        return null;
    }
}

wss.on("connection", function (connection) {
    console.log("user connected");

    connection.on("message", function (message) {
        var data;

        try {
            data = JSON.parse(message);
        }
        catch (e) {
            console.log("Invalid JSON");
            data = {};
        }

        switch (data.type) {
            case "login":
                console.log("User logged:", data.name);

                if (getUserByName(data.name)) {
                    console.log("already have that user");
                    sendTo(connection, {
                        type: "login",
                        success: false
                    });
                }
                else {
                    console.log("sending login success message");
                    users.push(new User(data.name, connection));
                    connection.name = data.name;

                    sendTo(connection, {
                        type: "login",
                        success: true
                    });
                }
                break;
            case "offer":
                console.log("Sending offer to: ", data.name);

                var conn = getUserConnectionByName(data.name);
                if (conn != null) {
                    connection.opponentName = data.name;
                    sendTo(conn, {
                        type: "offer",
                        offer: data.offer,
                        name: connection.name
                    });
                }
                break;
            case "answer":
                console.log("Sending answer to: ", data.name);

                var conn = getUserConnectionByName(data.name);
                if (conn != null) {
                    connection.opponentName = data.name;
                    sendTo(conn, {
                        type: "answer",
                        answer: data.answer
                    });
                }
                break;
            case "candidate":
                console.log("Sending candidate to: ", data.name);

                var conn = getUserConnectionByName(data.name);
                if (conn != null) {
                    sendTo(conn, {
                        type: "candidate",
                        candidate: data.candidate
                    });
                }
                else {
                    console.log("no user: " + data.name);
                }
                break;
            case "leave":
                console.log("Disconnecting from ", data.name);

                var conn = getUserConnectionByName(data.name);
                if (conn != null) {
                    conn.opponentName = null;
                    sendTo(conn, {
                        type: "leave"
                    });
                }
                break;
            case "users":
                var userList = [];
                for (var i = 0; i < users.length; ++i) {
                    var user = users[i];
                    if (user.name == name) {
                        userList.push(users[user].name);
                    }
                }
                sendTo(connection, {
                    type: "users",
                    users: userList
                });
                break;
            case "success":
                console.log(data.username + " successfully found an opponent.");

                var conn = getUserConnectionByName(data.username);
                if (conn != null) {
                    conn.terminate();
                }
                break;
            case "matchupFailed":
                console.log(data.username + " failed to connect to opponent.");

                for (var i = connectionAttemptList.length - 1; i >= 0; --i) {
                    var userPair = connectionAttemptList[i];
                    if ((userPair.firstUser == data.username || userPair.secondUser == data.username) &&
                        (userPair.firstUser == data.opponentName || userPair.secondUser == data.opponentName)) {
                        connectionAttemptList.splice(i, 1);
                        if (!connectionHasFailedPreviously(data.username, data.opponentName)) {
                            failedConnectionList.push(new UserPair(data.username, data.opponentName));
                        }
                        break;
                    }
                }
                break;
            default:
                sendTo(connection, {
                    type: "error",
                    message: "Command not found: " + data.type
                });
                break;
        }
    });

    connection.on("close", function () {
        console.log("user disconnected");
        if (connection.name) {
            users.splice(users.indexOf(getUserByName(connection.name)), 1);
            onUserRemoved(connection.name);
            console.log("Connection closed ", connection.name);

            if (connection.opponentName) {
                console.log("Disconnecting from ", connection.opponentName);


                var opponent = getUserByName(connection.opponentName);
                if (opponent != null) {
                    var conn = opponent.connection;
                    conn.opponentName = null;
                    sendTo(conn, {
                        type: "leave"
                    });
                }
            }
        }
    });

    // heartbeat
    connection.isAlive = true;
    connection.on('pong', heartbeat);
});

// heartbeat like in this example https://www.npmjs.com/package/ws#usage-examples




function noop() {}

function heartbeat() {
    this.isAlive = true;
}

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(connection) {
        if (connection.isAlive === false) {
            console.log("user timed out");
            return connection.terminate();   
        }
        connection.isAlive = false;
        connection.ping(noop);
    });
}, 30000);

// try to pair up two users randomly
var matchMakerLoop = setInterval(function () {
    //console.log("matchmaking...");
    //console.table(users);
    if (users.length > 1) {
        var usersToCheck = users.slice();
        while (usersToCheck.length > 0) {
            var user = usersToCheck.splice(0, 1)[0];
            //console.log("checking " + user.name);
            if (!userAlreadyAttempingConnection(user.name)) {
                for (var i = usersToCheck.length - 1; i >= 0; --i) {
                    var randomIndex = Math.floor(Math.random() * (i+1));
                    var compareUser = usersToCheck[randomIndex];
                    if (!userAlreadyAttempingConnection(compareUser.name) && !connectionHasFailedPreviously(user.name, compareUser.name)) {
                        // attempt to connect users
                        //console.log(user.name + " and " + compareUser.name + " connecting");
                        sendTo(user.connection, {
                            type: "matchup",
                            name: compareUser.name
                        });
                        connectionAttemptList.push(new UserPair(user.name, compareUser.name));
                        usersToCheck.splice(randomIndex, 1);
                        break;
                    }
                    else {
                        //console.log("can't connect to " + compareUser.name);
                        usersToCheck[randomIndex] = usersToCheck[i];
                        usersToCheck[i] = compareUser;
                    }
                }
            }
            else {
                //console.log("already attemping connection");
            }
        }
    }
}, 100);
//}, 2000);

var onUserRemoved = function (username) {
    for (var i = failedConnectionList.length - 1; i >= 0; --i) {
        var userPair = failedConnectionList[i];
        if (userPair.firstUser == username || userPair.secondUser == username) {
            failedConnectionList.splice(i, 1);
        }
    }
    for (var i = connectionAttemptList.length - 1; i >= 0; --i) {
        var userPair = connectionAttemptList[i];
        if (userPair.firstUser == username || userPair.secondUser == username) {
            connectionAttemptList.splice(i, 1);
        }
    }
};

function sendTo (connection, message) {
    connection.send(JSON.stringify(message));
}
