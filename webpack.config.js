var webpack = require("webpack");
var path = require("path");

module.exports = {
    entry: "./server.js",
    target: "node",
    output: {
        filename: "network-checkers-server.js",
        path: path.join(__dirname, "./dist")
    },
    module: {
        rules: [
        ]
    },
    plugins: [],
    resolve: {
        extensions: [
            ".js",
        ],
        modules: [
            "src",
            "node_modules"
        ]
    },
    devtool: "source-map"
};
